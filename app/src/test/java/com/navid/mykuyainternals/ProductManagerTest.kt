package com.navid.mykuyainternals

import com.navid.mykuyainternals.models.Category
import com.navid.mykuyainternals.models.Product
import com.navid.mykuyainternals.viewmodels.ProductsManager
import com.google.common.truth.Truth.assertWithMessage
import org.junit.Test

class ProductManagerTest {
    private val withIcon = "http://www.icon.test"
    private val withoutIcon = ""


    private val fprd1 = Product(1, "Featured Product 1", withoutIcon)
    private val fprd2 = Product(2, "Featured Product 2", withIcon)
    private val fprd3 = Product(3, "Featured Product 3", withoutIcon)
    private val fprd4 = Product(4, "Featured Product 4", withIcon)
    private val fprd5 = Product(4, "Featured Product 5", withIcon)
    private val fprd6 = Product(4, "Featured Product 6", withIcon)

    private val prd1 = Product(1, "Product 1", withIcon)
    private val prd2 = Product(2, "Product 2", withIcon)
    private val prd3 = Product(3, "Product 3", withIcon)
    private val prd4 = Product(4, "Product 4", withoutIcon)
    private val prd5 = Product(5, "Product 5", withoutIcon)
    private val prd6 = Product(6, "Product 6", withIcon)
    private val prd7 = Product(7, "Product 7", withIcon)
    private val prd8 = Product(8, "Product 8", withIcon)
    private val prd9 = Product(8, "Product 98", withIcon)

    var sut: ProductsManager? = null

    init {
        val featuredCat = Category("Featured")
        val allCat = Category("All Services")

        val cats: List<Category> = List(2) { i ->
            if (i == 0) {
                featuredCat
            } else {
                allCat
            }
        }


        featuredCat.products = List(6) { i ->
            when (i) {
                0 -> fprd1
                1 -> fprd2
                2 -> fprd3
                3 -> fprd4
                4 -> fprd5
                5 -> fprd5
                else -> fprd6
            }
        }
        allCat.products = List(9) { i ->
            when (i) {
                0 -> prd1
                1 -> prd2
                2 -> prd3
                3 -> prd4
                4 -> prd5
                5 -> prd6
                6 -> prd7
                7 -> prd8
                else -> prd9
            }
        }
        sut = ProductsManager()
        sut?.allCats = cats

    }

    @Test
    fun productManagerTest() {
        assertWithMessage("product Manager is null!").that(sut).isNotNull()
    }

//    @Test
//    fun getFeaturedCategoryTest() {
//        val featuredCategory = sut?.getFeaturedCategory()
//        assertWithMessage("featured category is null").that(featuredCategory).isNotNull()
//
//        assertWithMessage("featured category is not correct").that(featuredCategory)
//            .isEqualTo(Category("Featured"))
//    }
//
//    @Test
//    fun getFeaturedProductsTest() {
//        val featuredProducts = sut?.getFeaturedProducts()
//        assertWithMessage("featured products is null").that(featuredProducts).isNotNull()
//        assertWithMessage("featured products not correct").that(featuredProducts?.size).isEqualTo(6)
//    }
//
//    @Test
//    fun getDrawablesFeaturedProductsTest() {
//        val drawableFeaturedProducts = sut?.getDrawablesFeaturedProducts()
//        assertWithMessage("drawable featured products is null").that(drawableFeaturedProducts)
//            .isNotNull()
//        assertWithMessage("drawable featured products not correct").that(drawableFeaturedProducts?.size)
//            .isEqualTo(4)
//
//    }
//
//    @Test
//    fun getTopThreeFeaturedProductsTest() {
//        val topThreeFeaturedProducts = sut?.getTopThreeDrawablesFeaturedProducts()
//        assertWithMessage("top three featured products is null").that(topThreeFeaturedProducts)
//            .isNotNull()
//        assertWithMessage("top three featured products not correct").that(topThreeFeaturedProducts?.size)
//            .isLessThan(4)
//    }
//
//
//    @Test
//    fun getAllCategoryTest() {
//        val allCategory = sut?.getAllCategory()
//        assertWithMessage("all category is null").that(allCategory).isNotNull()
//
//        assertWithMessage("all category is not correct").that(allCategory)
//            .isNotEqualTo(Category("Featured"))
//    }
//
//    @Test
//    fun getAllProductsTest() {
//        val allProducts = sut?.getAllProducts()
//        assertWithMessage("all products is null").that(allProducts).isNotNull()
//        assertWithMessage("all products not correct").that(allProducts?.size).isEqualTo(9)
//    }
//
//    @Test
//    fun getDrawablesAlldProductsTest() {
//        val drawableAllProducts = sut?.getDrawablesAllProducts()
//        assertWithMessage("drawable all products is null").that(drawableAllProducts)
//            .isNotNull()
//        assertWithMessage("drawable all products not correct").that(drawableAllProducts?.size)
//            .isEqualTo(7)
//    }
//
//    @Test
//    fun getDrawablesAlldProductsCompactTest() {
//        val drawableAllProducts = sut?.getDrawablesAllProducts(true)
//        assertWithMessage("drawable all products compact is null").that(drawableAllProducts)
//            .isNotNull()
//        assertWithMessage("drawable all products compact not correct").that(drawableAllProducts?.size)
//            .isAtMost(6)
//    }

    @Test
    fun getFeaturedProductSizeTest() {
        val featuredCount = sut?.getFeaturedProductsCount()
        assertWithMessage("Featured products size is incorrect").that(featuredCount).isAtMost(3)
    }

    @Test
    fun getAllProductSizeTest() {
        val featuredCount = sut?.getAllProductsCount()
        assertWithMessage("All products size is incorrect").that(featuredCount).isEqualTo(7)
    }

    @Test
    fun getAllProductSizeCompactTest() {
        val featuredCount = sut?.getAllProductsCount(true)
        assertWithMessage("All products of compact list size is incorrect").that(featuredCount)
            .isAtMost(6)
    }

    @Test
    fun getFeaturedProductAtIndexTest() {
        val nullFeaturedProduct = sut?.getFeaturedProductAtIndex(3)
        assertWithMessage("featured product should be null").that(nullFeaturedProduct).isNull()

        val featuredProduct = sut?.getFeaturedProductAtIndex(2)
        assertWithMessage("get Index not correct for Featured Product").that(featuredProduct)
            .isEqualTo(fprd5)

    }

    @Test
    fun getAllProductAtIndexCompactTest() {
        val nullFeaturedProduct = sut?.getAllProductAtIndex(6, true)
        assertWithMessage("product should be null").that(nullFeaturedProduct).isNull()

        val featuredProduct = sut?.getAllProductAtIndex(2, true)
        assertWithMessage("get Index not correct for the Product").that(featuredProduct)
            .isEqualTo(prd3)

    }

    @Test
    fun getAllProductAtIndexTest() {
        val nullFeaturedProduct = sut?.getAllProductAtIndex(6)
        assertWithMessage("product should not be null").that(nullFeaturedProduct).isNotNull()

        val featuredProduct = sut?.getAllProductAtIndex(6)
        assertWithMessage("get Index not correct for the Product").that(featuredProduct)
            .isEqualTo(prd9)

    }
}