package com.navid.mykuyainternals

import com.navid.mykuyainternals.models.News
import org.junit.Test

import com.google.common.truth.Truth.assertWithMessage

class NewsTest {

    @Test
    fun newsTest() {
        val firstNews = News(R.drawable.news_hta, "How To Use App", "Getting access to on-demand")
        val secondNews =
            News(R.drawable.news_lysomk, "List your services on MyKoya", "Do You Offer Manpower")

        assertWithMessage("news is ready").that(firstNews).isNotNull()
        assertWithMessage("second news as ready").that(secondNews).isNotNull()
    }
}