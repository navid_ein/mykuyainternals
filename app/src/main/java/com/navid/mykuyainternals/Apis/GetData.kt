package com.navid.mykuyainternals.Apis

import com.navid.mykuyainternals.models.Category
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GetData {


    @Headers("Authorization: $Token")
    @GET("client/v1/product/catalog")
    fun getProducts(
        @Query("lat") lat: Double,
        @Query("lng") lng: Double
    ): Observable<List<Category>>


    companion object {
        const val Token = """
            Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjU3MCwicGFydHlJZCI6MjMyLCJpYXQiOjE1OTU0ODAzMjcsImV4cCI6MTYyNzAxNjMyN30.MB_yb9_vvG293ipl0AJ3RslII451igicfarei1XlXVQ
        """
        const val BaseUrl = "https://client.dev.mykuyainternals.com/"

        fun createAPI(): GetData {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BaseUrl)
                .build()
            return retrofit.create(GetData::class.java)

        }
    }


}