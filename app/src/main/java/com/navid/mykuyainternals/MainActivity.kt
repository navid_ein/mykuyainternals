package com.navid.mykuyainternals

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.navid.mykuyainternals.Adapters.FeaturedProductsAdapter
import com.navid.mykuyainternals.Adapters.NewsAdapter
import com.navid.mykuyainternals.Adapters.ProductsAdapter
import com.navid.mykuyainternals.Apis.GetData
import com.navid.mykuyainternals.models.Category
import com.navid.mykuyainternals.models.News
import com.navid.mykuyainternals.viewmodels.NewsManager
import com.navid.mykuyainternals.viewmodels.ProductsManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null
    private var newsDisposable: Disposable? = null
    private var isCompact = true
    private lateinit var productsManager: ProductsManager
    private lateinit var newsManager: NewsManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        productsManager = ProductsManager()
        newsManager = NewsManager()

        getProducts(35.6892, 51.3890)
        initNews()
    }

    private fun getProducts(lat: Double, lng: Double) {
        disposable = productsManager.getProducts(lat, lng).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> showResult(result) },
                { error -> showError(error.message) }
            )
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
        newsDisposable?.dispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
        newsDisposable?.dispose()
    }

    private fun showResult(cats: List<Category>) {
        Log.d("TAG", "size of cats : ${cats.size}")
        mainProgressBar.visibility = View.INVISIBLE
        productsManager.allCats = cats
        val featuredAdapter =
            FeaturedProductsAdapter(productsManager, R.layout.featured_product_item)
        featuredRecyclerView.layoutManager = GridLayoutManager(applicationContext, 3)
        featuredRecyclerView.adapter = featuredAdapter

        val allAdapter = ProductsAdapter(productsManager, R.layout.product_item)
        allRecyclerView.layoutManager = GridLayoutManager(applicationContext, 3)
        allRecyclerView.adapter = allAdapter

    }

    private fun showError(msg: String?) {
        Log.d("TAG", "size of cats : $msg")
    }

    fun onMoreClicked(view: View) {
        isCompact = !isCompact
        setMoreState()
    }

    private fun setMoreState() {
        (allRecyclerView.adapter as ProductsAdapter).isCompact = isCompact
        (allRecyclerView.adapter as ProductsAdapter).notifyDataSetChanged()
        val drwIcon = if (isCompact) R.drawable.ic_open else R.drawable.ic_close
        moreButton.setImageResource(drwIcon)
    }

    private fun initNews() {
        newsDisposable = newsManager.getNews().subscribe { result ->
            showNews(result)
        }
    }

    private fun showNews(news: Array<News>) {
        val newsAdapter = NewsAdapter(news, R.layout.news_item)
        newsRecyclerView.layoutManager =
            LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        newsRecyclerView.adapter = newsAdapter
    }


}
