package com.navid.mykuyainternals.models

class Category(private var categoryName: String) {

    var products: List<Product>? = null

    override operator fun equals(other: Any?): Boolean {
        return if (other is Category) other.categoryName == this.categoryName else
            false
    }

    override fun hashCode(): Int {
        return categoryName.hashCode()
    }

}