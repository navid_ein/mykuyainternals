package com.navid.mykuyainternals.models

import com.google.gson.annotations.SerializedName

class Product(
    private var id: Int,
    @SerializedName("commercialName") var name: String,
    var icon: String
) {

//    var id: Int = -1

    var minPoints: Int = 0
    var maxPoints: Int = 0


    /* "id": 13,
     "commercialName": "Personal Assistant",
     "icon": "https://mykuya-products-stage.s3.ap-southeast-1.amazonaws.com/product/13-icon.jpg",
     "minPoints": 1,
     "maxPoints": 10,
     "isTwoWay": false,
     "supportedGeoIds": [],
     "categoryTags": [],
     "customTags": []

     */
}