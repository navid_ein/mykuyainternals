package com.navid.mykuyainternals.viewmodels

import com.navid.mykuyainternals.Apis.GetData
import com.navid.mykuyainternals.models.Category
import com.navid.mykuyainternals.models.Product
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*

class ProductsManager() {
    companion object {
        const val compactSize = 6
    }

//    val resultReadyPublisher = PublishSubject.create<List<Category>>()
//    val resultErrorPublisher = PublishSubject.create<String>()

    private val getAPI by lazy {
        GetData.createAPI()
    }
    lateinit var allCats: List<Category>

    private val featuredCategory: Category by lazy {
        allCats.filter { it == Category("Featured") }[0]
    }
    private val allCategory: Category by lazy {
        allCats.filter { it != Category("Featured") }[0]
    }

    private fun getFeaturedProducts(): List<Product>? {
        return featuredCategory.products
    }

    private fun getDrawableFeaturedProducts(): List<Product>? {
        return getFeaturedProducts()?.filter { it.icon.length > 5 }
    }

    private fun getTopThreeFeaturedProducts(): List<Product>? {
        return getDrawableFeaturedProducts()?.take(3)
    }

    //    private val featuredProducts: List<Product>? = featuredCategory.products
//    private val drawableFeaturedProducts: List<Product>? =
//        featuredProducts?.filter { it.icon.length > 5 }
//    private val topThreeFeaturedProducts: List<Product>? = drawableFeaturedProducts?.take(3)


    private fun getAllProducts(): List<Product>? {
        return allCategory.products
    }

    private fun getDrawableAllProducts(): List<Product>? {
        return getAllProducts()?.filter { it.icon.length > 5 }
    }

    private fun getCompactDrawableProducts(): List<Product>? {
        return getDrawableAllProducts()?.take(6)
    }
//    private val allProducts: List<Product>? = allCategory.products
//    private val drawableAllProducts: List<Product>? = allProducts?.filter { it.icon.length > 5 }
//    private val compactDrawableProducts: List<Product>? = drawableAllProducts?.take(6)

    fun getProducts(lat: Double, lng: Double): Observable<List<Category>> {
        return getAPI.getProducts(lat, lng)
    }


    fun getFeaturedProductsCount(): Int {
        return getTopThreeFeaturedProducts()?.size ?: 0
    }

    fun getAllProductsCount(compact: Boolean = false): Int {
        return if (compact) getCompactDrawableProducts()?.size ?: 0
        else
            getDrawableAllProducts()?.size ?: 0
    }

    fun getFeaturedProductAtIndex(index: Int): Product? {
        return if (index < 3)
            getTopThreeFeaturedProducts()?.get(index)
        else
            null
    }

    fun getAllProductAtIndex(index: Int, compact: Boolean = false): Product? {

        return if (compact && index < getAllProductsCount(compact)) {
            getCompactDrawableProducts()?.get(index)
        } else if (!compact && index < getAllProductsCount(compact)) {
            getDrawableAllProducts()?.get(index)
        } else {
            null
        }

    }

}