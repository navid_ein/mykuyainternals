package com.navid.mykuyainternals.viewmodels

import com.navid.mykuyainternals.R
import com.navid.mykuyainternals.models.Category
import com.navid.mykuyainternals.models.News
import io.reactivex.Observable

class NewsManager {


    fun getNews(): Observable<Array<News>> {
        return Observable.create<Array<News>>()
        {
            val firstNews =
                News(R.drawable.news_hta, "How To Use App", "Getting access to on-demand")
            val secondNews =
                News(
                    R.drawable.news_lysomk,
                    "List your services on MyKoya",
                    "Do You Offer Manpower"
                )
            var news: Array<News> = Array(2) { i ->
                when (i) {
                    0 -> firstNews
                    else -> secondNews
                }
            }
            it.onNext(news)
            it.onComplete()

        }
    }
}