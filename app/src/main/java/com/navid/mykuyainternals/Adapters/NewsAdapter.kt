package com.navid.mykuyainternals.Adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.navid.mykuyainternals.models.News
import kotlinx.android.synthetic.main.news_item.view.*


class NewsAdapter(private var news: Array<News>, private var layout: Int) :
    RecyclerView.Adapter<NewsAdapter.ProductHolder>() {

    class ProductHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView


        fun bind(news: News) {
            view.newsTitle.text = news.title
            view.newsDescription.text = news.description
            view.newsImage.setImageResource(news.image)


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val inflatedView = parent.inflate(layout)
        return ProductHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val currentNews = news.get(position)
        holder.bind(currentNews)

    }

    override fun getItemCount(): Int {
        return news.size
    }

}