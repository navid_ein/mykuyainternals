package com.navid.mykuyainternals.Adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.navid.mykuyainternals.models.Product
import com.navid.mykuyainternals.viewmodels.ProductsManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_item.view.*


class FeaturedProductsAdapter(
    private var productsManager: ProductsManager,
    private var layout: Int
) :
    RecyclerView.Adapter<FeaturedProductsAdapter.ProductHolder>() {

    class ProductHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView


        fun bind(product: Product) {
            Picasso.get().load(product.icon).into(view.productImage)
            view.productName.text = product.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val inflatedView = parent.inflate(layout)
        return ProductHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val product = productsManager.getFeaturedProductAtIndex(position)
        if (product != null)
            holder.bind(product)

    }

    override fun getItemCount(): Int {
        return productsManager.getFeaturedProductsCount()
    }

}